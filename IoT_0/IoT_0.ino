/*
 * Questo programma e' il primo della serie per il tutorial IoT
 * del corso di telematica.
 * E' il "solito" blink, con una piccola sorpresa nel loop
 */
#define LED 2 // Il LED sulla scheda e' collegato al pin 2

// Questa funzione viene eseguita una sola volta all'accensione
void setup() {
  // definisce la modalita' del pin 2 (LED) come output
  pinMode(LED,OUTPUT);
  // quando il pin viene messo a livello basso il LED si accende
  digitalWrite(LED,LOW);
  // per 5 secondi
  delay(5000); // ritardo in millesecondi
}

// Questa funzione viene ripetuta indefinitamente
// Terminato il setup il LED si accende 1 volta al secondo
void loop() {
  digitalWrite(LED,!(digitalRead(LED)));  // inverto lo stato del LED
  delay(500);                             // Attendo mezzo secondo
}
